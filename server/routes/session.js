module.exports = function(app) {
    app.get('/dumpSession', function(req, res) {
        if (!req.session.views) {
            req.session.views = 1;
        } else {
            req.session.views += 1;
        }

        res.json({
            "status": "ok",
            "frequency": req.session.views
        });
    });    
};
