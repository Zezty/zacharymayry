// Raspberry pi camera
module.exports = function(app) {
    var fs = require('fs'),
        path = require('path'), 
        raspiCam = require('raspicam');

    fs.existsSync(path.join(app.zroot, 'raspicam')) || fs.mkdirSync(path.join(app.zroot, 'raspicam'));
    var camPath = path.join(app.zroot, 'raspicam', 'cam.jpg');
    var cam = new raspiCam({
        'mode': 'photo',
        'output': camPath,
        'encoding': 'jpg'
    });
    
    app.get('/cam', function(req, res) {
        cam.timestampms = (new Date()).getMilliseconds();
        if (app.isProd) {
            try {
                cam.start();
                cam.on('read', function (err) {
                    cam.stop(); // I think this is for video
                    // if (err) logger.error(util.timeStamp() + '\n' + err + '\n');
                    fs.readFile(camPath, function (err, data) {
                        // if (err) logger.error(util.timeStamp() + '\n' + err + '\n');
                        // TODO: Establish socket connection and stream it
                        res.end(data, 'image/jpeg');
                    });
                });
            } catch(err) {
                // if (err) logger.error(util.timeStamp() + '\n' + err + '\n');
                res.end('An error has occured' ,'utf-8');
            }
        } else {
            res.write("Come up with something witty later.....");
            res.end('\nAm I doing this right?' ,'utf-8');
        }
    });
};




