module.exports = function(app) {
    var util = require('../functions');
    var urlencodedParser = require('body-parser').urlencoded({ extended: false });

    // TODO: login validation and redireciton middleware
    var code = util.genCode();
    // Create a new REST API client to make authenticated requests against the twilio back end
    var client = new require('twilio').RestClient('AC92c433b42c57ef2d13dacb4bb9ff876a', '607effa4aefc092a3e2d6452064f008d');
    
    app.get('/login', function(req, res) {
        code = util.genCode();
        client.sms.messages.create({
            to:'+12187804891',
            from:'+16123159918',
            body:"Secret, Secret, I've got a secret " + code
        }, function(error, message) {
            if (!error) {
                console.log('Success! The SID for this SMS message is:');
                console.log(message.sid);

                console.log('Message sent on:');
                console.log(message.dateCreated);
            } else {
                console.log('Oops! There was an error.');
            }
        });

        res.render( 'login', {
            title: 'The risky click of the day goes to',
            errormsg: ''
        });
    });

    app.post('/login', urlencodedParser, function(req, res) {
        if (req.body.secret == code) {
            res.redirect('/ua');
        } else {
            res.render( 'login', {
                title: 'The risky click of the day goes to',
                errormsg: 'Your secret key is incorrect'
            });
        }
    });
};
