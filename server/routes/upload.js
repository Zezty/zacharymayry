module.exports = function(app) {
    var upload = require('multer')({dest: './tmp/'}),
        fs = require('fs'),
        path = require('path'),
        logger = require('../logger');

    fs.existsSync(path.join(app.zroot, 'uploads')) || fs.mkdirSync(path.join(app.zroot, 'uploads'));
    fs.existsSync(path.join(app.zroot, 'uploads', 'images')) || fs.mkdirSync(path.join(app.zroot, 'uploads', 'images'));
    
    app.get('/upload', function(req, res) {
        res.render( 'uploads', {
            title: 'Upload Literally Anything'
        });
    });
    app.post('/upload', upload.array('upload'), function(req, res) {
        var images = new Array(req.files.length);
        req.files.forEach(function (file, index) {
            var extension = file.originalname.split('.').pop();
            if (file.mimetype.includes('image')) {
                fs.readFile(file.path, function (err, data) {
                    setUpImagePath(extension, file.mimetype, index, function (imagePath, mimetype) {
                        images[index] = 'data:' + mimetype + ';base64,' + new Buffer(data).toString('base64');
                        fs.writeFile(imagePath, data, function (err) {
                            if (err) logger.error(dt + "\n" + err);
                            else {
                                fs.unlink(file.path, function () {
                                    if (index === req.files.length - 1) {
                                        res.render('uploads', {
                                            title: 'Upload Literally Anything Else',
                                            images: images
                                        });
                                    }
                                });
                            }
                        });
                    });
                });
            }

            // } else if (file.mimetype.includes('archive')) {
            // TODO: Extract archive, recursively handle each file.

            // } else if (file.mimetype.includes('torrent')) {
            // TODO: Send to torrent mule, set a scheduled task 5-15 min from now that wills start openvpn, then start transmission

            // epub along with pdf and docs 42 pages or longer go in books
            // all other pdfs and docs go in documents

            // come up with something cool for music

            // videos can just go in the videos folder
            // sort based on some naming convention

            // text files might be considered executable. TODO: Test this
            // run scripts, check security on this. I'd hate to lose a shitload of stuff.
            // yada yada
            else {
                fs.existsSync(path.join(app.zroot, 'uploads', extension)) || fs.mkdirSync(path.join(app.zroot, 'uploads', extension));
                fs.rename(file.path, path.join(app.zroot, 'uploads', extension, file.originalname), function() {
                    res.render('uploads', {
                        title: 'Upload Literally Anything Else',
                        success: 'Files uploaded successfully'
                        // TODO: loop and provide files that were uploaded for reference.
                    });
                });
            }
        });
    });

    // sets up image folders based on dates.
    // Returns the path of the image just uploaded
    function setUpImagePath(extension, mimetype, index, callback) {
        var dateFormat = require('dateformat');
        var now = new Date();
        var dt = dateFormat(now, 'yyyymmdd_hhMMssl');
        var year = now.getFullYear().toString();
        var month;
        if (now.getMonth() < 9)
            month = '0' + (now.getMonth()+1).toString();
        else {
            month = (now.getMonth() + 1).toString();
        }

        // create directories to store images if they don't already exist.
        fs.existsSync(path.join(app.zroot, 'uploads', 'images', year)) || fs.mkdirSync(path.join(app.zroot, 'uploads', 'images', year));
        fs.existsSync(path.join(app.zroot, 'uploads', 'images', year, month)) || fs.mkdirSync(path.join(app.zroot, 'uploads', 'images', year, month));

        // format index with a leading zero for more exact time based naming.
        if (index < 10) index = '0'+index.toString();

        var imagePath = path.join(app.zroot, 'uploads/images', year, month, dt + index + '.' + extension);
        console.log(imagePath);
        callback(imagePath, mimetype);
    }
};