module.exports = function(dirname) {
    var express = require('express'),
        fs = require('fs'),
        path = require('path'),
        // TODO: Set up more logging for use in "production"/pm2
        // TODO: Set up better folder structure for server side js
        logger = require('./logger');

    // set up express
    var app = express();
    app.zroot = dirname;
    app.zlogs = path.join(app.zroot, 'logs');
    app.ztmp = path.join(app.zroot, 'tmp');

    // Production or dev/staging
    var ip = require('ip');
    app.ip = ip.address();
    app.isProd = (app.ip === '192.168.1.3');

    // ensure directories exist
    fs.existsSync(app.zlogs) || fs.mkdirSync(app.zlogs);
    fs.existsSync(app.ztmp) || fs.mkdirSync(app.ztmp);

    // set up pug (jade)
    var pug = require('pug');
    app.set('views', app.zroot + "/views");
    app.set('view engine', 'pug');
    app.locals.basedir = app.zroot;

    // Set up postgres database;
    var pg = require('pg');

    // set up session
    var session = require('express-session');
    var pgsession = require('connect-pg-simple')(session);
    var sessionOptions = {
        secret: 'turbo',
        resave : false,
        saveUninitialized : false,
        store : new pgsession({
            pg: pg,
            conString: "postgres://pi:not2That!One&Dummy@localhost:5432/pi",
            tableName: 'session'
        })
    };
    app.use(session(sessionOptions));

    // include available routes
    require('./routes/cam')(app);
    require('./routes/login')(app);
    require('./routes/session')(app);
    require('./routes/upload')(app);

    // set up static files
    var serveIndex = require('serve-index');
    app.use('/uploads', express.static('uploads'));
    app.use('/uploads/', serveIndex(path.join(app.zroot, '/uploads/'), {'icons': true}));
    app.use('/', express.static('about'));

    app.listen( 8081, function() {
        console.log("Listening on port 8081");
    });
};