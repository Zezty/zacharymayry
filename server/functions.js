var dateFormat = require('dateformat'),
    fs = require('fs'),
    path = require('path');

module.exports = {
    // mfa, code, secret, what have you
    // TODO: get a library for this...
    genCode: function () {
        return Math.floor(Math.random() * 500000 +1);
    },
    timeStamp: function() {
        return dateFormat(new Date(), 'yyyymmdd_hhMMssl');
    }
};