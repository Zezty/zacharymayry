var path = require( 'path' );
var winston = require( 'winston' );
winston.transports.DailyRotateFile = require( 'winston-daily-rotate-file' );

var logger = new winston.Logger({
    transports: [
        new winston.transports.DailyRotateFile({
            name : 'error-file',
            level : 'error',
            filename: path.join( __dirname, '..', '/logs/error' ),
            datePattern : '-yyyyMMdd.log'
            //handleExceptions : true,
            //humanReadableUnhandledException : false
        }),
        new winston.transports.DailyRotateFile({
            name : 'info-file',
            level : 'info',
            filename : path.join( __dirname, '..', '/logs/info'),
            datePattern : '-yyyyMMdd.log'
        })
    ]
});

module.exports = logger;
